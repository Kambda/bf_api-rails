module BfApi::LoginableConcern
  def self.included(base)
    base.instance_eval do
      concern :loginable do
        collection do
          post 'sign_in' => 'users#sign_in_resource'
          get  'me'
        end
      end
    end
  end
end

module BfApi::FacebookLoginableConcern
  def self.included(base)
    base.instance_eval do
      concern :facebook_loginable do
        collection do
          post 'facebook_sign_in'
        end
      end
    end
  end
end

module BfApi::GoogleLoginableConcern
  def self.included(base)
    base.instance_eval do
      concern :google_loginable do
        collection do
          post 'google_sign_in'
        end
      end
    end
  end
end
require 'rails/generators'

module BfApiFacebookLogin
  class MigrationGenerator < ::Rails::Generators::NamedBase
    desc "Create a Facebook Login migration for the given class name"
    source_root File.expand_path('../templates', __FILE__)

    def create_migration_file
      destination = "db/migrate/#{Time.now.utc.strftime('%Y%m%d%H%M%S')}_bf_api_facebook_login_migration_for_#{file_name}.rb".gsub(" ", "")
      migration_name = "BfApiFacebookLoginMigrationFor#{file_name.titlecase}".gsub(" ", "")
      count = nil
      i = 1
      while !Dir.glob("db/migrate/*_bf_api_facebook_login_migration_for_#{file_name}#{count}.rb".gsub(" ", "")).empty?
        i += 1
        count = "_#{i}"
        destination = "db/migrate/#{Time.now.utc.strftime('%Y%m%d%H%M%S')}_bf_api_facebook_login_migration_for_#{file_name}#{count}.rb".gsub(" ", "")
        migration_name = "BfApiFacebookLoginMigrationFor#{file_name.titlecase}#{i}".gsub(" ", "")
      end
      create_file destination, <<-FILE
class #{migration_name} < ActiveRecord::Migration
  def change
    add_column :#{plural_name}, :facebook_id, :string
    add_index :#{plural_name}, :facebook_id
  end
end
FILE
    end
  end
end

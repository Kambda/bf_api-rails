$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "bf_api-rails/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "bf_api-rails"
  s.version     = BfApi::VERSION
  s.authors     = ["Tod Birdsall"]
  s.email       = ["todb@brushfireinteractive.com"]
  s.homepage    = "http://brushfireinteractive.com"
  s.summary     = "Summary of BfApi."
  s.description = "Description of BfApi."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.1"
  s.add_dependency "jbuilder", "~> 2.0" # Error Display
  s.add_dependency "anaconda", "~> 1.0" # Panel Editing
end

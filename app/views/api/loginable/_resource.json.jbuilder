skip_auth_token ||= false
json.cache! ["v1", resource, (resource == send("current_#{resource.class.name.underscore}"))] do
  json.(resource, :id)
  json.username resource.username if resource.class.method_defined? :username
  if resource == send("current_#{resource.class.name.underscore}") && !skip_auth_token
    json.authentication_token resource.authentication_token
    json.email resource.email
  end
end
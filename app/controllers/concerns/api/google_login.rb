require 'active_support/concern'
module Api::GoogleLogin
  extend ActiveSupport::Concern
  include Api::JSONErrors

  included do
    skip_before_filter :authenticate_resource_from_token!, :authenticate_user!, only: [:google_sign_in]

    @@resource ||= User
  end

  def google_sign_in
    if params[:access_token].blank?
      render_error "No Google Access token supplied", 409
      return
    end

    uri = URI.parse("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=#{CGI.escape params[:access_token]}")
    response = Net::HTTP.get_response(uri)
    json = JSON.parse response.body

    if json["error_description"] != nil || json["sub"].blank? || json["email"].blank?
      render_error "Invalid Access token supplied", 409
      return
    end

    if resource = @@resource.find_by(google_id: json["sub"])
      # Found a matching resource by google_id
      @resource = resource
    elsif resource = @@resource.find_by(email: json["email"])
      # Found a matching resource by email
      @resource = resource
      @resource.update_attribute(:google_id, json["sub"])
      @resource.update_attribute(:profile_picture, json["picture"])
    else
      # Create a new resource
      @resource = @@resource.new(
          google_id: json["sub"],
          username: nil,
          email: json["email"],
          first_name: json["given_name"],
          last_name: json["family_name"],
          profile_picture: json["picture"],
          password: Devise.friendly_token
      )
      if !@resource.save
        render_error @resource.errors.full_messages, 409
        return
      end
    end
    sign_in @resource
    render "api/loginable/show"
  end
end
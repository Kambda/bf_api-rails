require 'active_support/concern'
module Api::Login
  extend ActiveSupport::Concern
  include Api::JSONErrors
  
  included do
    skip_before_filter :authenticate_resource_from_token!, :authenticate_user!, only: [:sign_in_resource]
  
    @@resource ||= User
  end
  
  def sign_in_resource # Can't be named sign_in as that breaks all things devise
    resource = @@resource.find_by(email: resource_params[:email]) || @@resource.find_by(username: resource_params[:email])
    if !resource_params[:email].nil? && resource && resource.valid_password?(resource_params[:password])
      sign_in resource
      @resource = resource
      render "api/loginable/show"
    else
      render_error "Invalid Username or Password", 409
    end
  end
  
  def me
    @resource = send("current_#{@@resource.name.underscore.to_sym}")
    render "api/loginable/show"
  end
  
  def show
    @resource = @@resource.find(params[:id])
    render "api/loginable/show"
  end
  
  private
  
  def resource_params
    params.require(@@resource.name.underscore.to_sym).permit(
      :email,
      :username,
      :current_password,
      :password
    )
  end
  
end
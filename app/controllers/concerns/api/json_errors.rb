require 'active_support/concern'
module Api::JSONErrors
  extend ActiveSupport::Concern

  def render_error(messages, status)
    messages = [messages] if messages.kind_of? String
    @messages = messages
    Rails.logger.debug "Error Message: (status: #{status})#{@messages}"
    if status >= 200 && status <= 299
      raise "Try again. You can't pass a 2xx status with an error."
    end
    render template: "api/shared/errors", status: status
  end
end
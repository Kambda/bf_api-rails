require 'active_support/concern'
module Api::FacebookLogin
  extend ActiveSupport::Concern
  include Api::JSONErrors
  
  included do
    skip_before_filter :authenticate_resource_from_token!, :authenticate_user!, only: [:facebook_sign_in]
  
    @@resource ||= User
  end
  
  def facebook_sign_in
    if params[:access_token].blank?
      render_error "No Facebook Access token supplied", 409
      return
    end
    
    uri = URI.parse("https://graph.facebook.com/me?fields=id,email,name,picture,first_name,last_name&access_token=#{CGI.escape params[:access_token]}")
    response = Net::HTTP.get_response(uri)
    json = JSON.parse response.body
    
    if json["error"] != nil || json["id"].blank? || json["email"].blank?
      render_error "Invalid Access token supplied", 409
      return
    end
    
    if resource = @@resource.find_by(facebook_id: json["id"])
      # Found a matching resource by facebook_id
      @resource = resource
    elsif resource = @@resource.find_by(email: json["email"])
      # Found a matching resource by email
      @resource = resource
      @resource.update_attribute(:facebook_id, json["id"])
    else
      # Create a new resource
      @resource = @@resource.new(
        facebook_id: json["id"],
        username: nil,
        email: json["email"],
        first_name: json["first_name"],
        last_name: json["last_name"],
        password: Devise.friendly_token
      )
      if !@resource.save
        render_error @resource.errors.full_messages, 409
        return
      end
    end
    sign_in @resource
    render "api/loginable/show"
  end
end
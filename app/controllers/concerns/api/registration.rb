require 'active_support/concern'
module Api::Registration
  extend ActiveSupport::Concern
  include Api::JSONErrors
  include Api::Login
  
  included do
    skip_before_filter :authenticate_resource_from_token!, :authenticate_user!, only: :create
  
    @@resource ||= User
  end
  
  def create
    @resource = @@resource.new(resource_params)
    if @resource.save
      sign_in @resource
      render template: "api/loginable/show"
    else
      render_error @resource.errors.full_messages, 409
    end
  end
  
  
end
require 'active_support/concern'
module Api::TokenAuthentication
  extend ActiveSupport::Concern
  include Api::JSONErrors
  
  included do
    # This is our new function that comes before Devise's one
    before_filter :authenticate_resource_from_token!
    # This is Devise's authentication
    #before_filter :authenticate_user!
    
    respond_to :json
    
    @@TokenResource ||= User
  end

  private
  def authenticate_resource_from_token!
    Rails.logger.debug "authenticate_resource_from_token!"
    authenticate_with_http_token do |token, options|
      Rails.logger.debug "token: #{token}"
      resource = @@TokenResource.find_by(authentication_token: token) unless token.nil?
      if resource
        # Notice we are passing store false, so the user is not
        # actually stored in the session and a token is needed
        # for every request.
        sign_in resource, store: false
        return
      end
    end
    Rails.logger.debug "No token, or invalid token"
    self.headers["WWW-Authenticate"] = %(Token realm="Application")
    render_error "Invalid Authentication Token", 401
  end
  
end
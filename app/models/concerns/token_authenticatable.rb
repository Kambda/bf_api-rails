require 'active_support/concern'
module TokenAuthenticatable
  extend ActiveSupport::Concern
  
  included do
    before_save :update_authentication_token
    before_save :ensure_authentication_token
  end

  def update_authentication_token
    # Update the authentication token whenever the password is changed
    return unless self.encrypted_password_changed?
    self.authentication_token = generate_authentication_token
  end

  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end

  private
  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless self.class.find_by(authentication_token: token)
    end
  end
end
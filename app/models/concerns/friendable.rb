require 'active_support/concern'
module Friendable
  extend ActiveSupport::Concern

  def request_friend(user_id)
    if user_id.kind_of?(User) # If you pass a user, this should still work
      user_id = user_id.id 
    else
      # Let's verify this is actually a user
      raise "Invalid user id" unless User.find(user_id).present?
    end
    
    if user_id == self.id
      # You can't friend yourself..
      return false
    end
    
    existing_or_pending_friendships = Friendship.where("(left_user_id = ? AND right_user_id = ?) OR (right_user_id = ? AND left_user_id = ?)", self.id, user_id, self.id, user_id)
    
    if existing_or_pending_friendships.present?
      existing_or_pending_friendships.each do |friendship|
        if friendship.left_user_id == user_id && friendship.right_user_id == self.id && friendship.pending?
          # This is a request from the other user
          friendship.accepted!
        elsif friendship.left_user_id == self.id && friendship.right_user_id == user_id
          # This is a duplicate request
          return false
        end
      end
    else
      Friendship.create(left_user_id: self.id, right_user_id: user_id)
    end
  end
  
  def accept_friend_request_from(user_id)
    request_friend(user_id)
  end
  
  def ignore_friend_request_from(user_id)
    if user_id.kind_of?(User) # If you pass a user, this should still work
      user_id = user_id.id 
    end
    
    friendship = Friendship.find_by(left_user_id: user_id, right_user_id: self.id, status: Friendship.statuses[:pending])
    if friendship
      friendship.ignored!
    end
  end

  def remove_friend(user_id)

    existing_friendships = Friendship.where("(left_user_id = ? AND right_user_id = ?) OR (right_user_id = ? AND left_user_id = ?)", self.id, user_id, self.id, user_id)
    if existing_friendships.present?
      existing_friendships.each do |friendship|
        friendship.destroy
      end
    else
      # Friendship doesn't exist
      false
    end
  end
  
  def requested_friend_ids
    Friendship.where(left_user_id: self.id, status: [Friendship.statuses[:pending], Friendship.statuses[:ignored]]).collect{|f| f.right_user_id}.uniq.reject{|i| i == self.id}
  end
  
  def requested_friends
    User.where(id: requested_friend_ids)
  end
  
  def pending_friend_request_ids
    Friendship.where(right_user_id: self.id, status: Friendship.statuses[:pending]).collect{|f| f.left_user_id}.uniq.reject{|i| i == self.id}
  end
  
  def pending_friend_requests
    User.where(id: pending_friend_request_ids)
  end
  
  def friend_ids
    Friendship.where("(left_user_id = ? OR right_user_id = ?) AND friendships.status = ?", self.id, self.id, Friendship.statuses[:accepted]).collect{|f| [f.left_user_id, f.right_user_id]}.flatten.uniq.reject{|i| i == self.id}
  end
  
  def friends
    User.where(id: friend_ids)
  end
end